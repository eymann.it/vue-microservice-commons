import axios from "axios";
import { initialState, mutations, actions, getters } from "./store.js"

const services = {
    auth: {
        url: '/users/v3/auth',
        endpoints: {
            login: { url: '/login/', protection: 'basic' },
            refresh: { url: '/refresh/', protection: 'refresh' },
            logout: { url: '/logout/', protection: 'jwt'}
        }
    },
    users: {
        url: '/users/v3/users',
        endpoints: {
            me: { url: '/me/', protection: 'jwt' },
            setAccountPassword: { url: '/me/set-password/', protection: 'basic' },
            users: { url: '/', protection: 'jwt' },
            user: { url: '/:userId/', protection: 'jwt' },
            setPassword: { url: '/:userId/set-password/', protection: 'jwt' },
        }
    }
};

function assert (condition, msg) {
    if (!condition) throw new Error(`[repository] ${msg}`)
}

class RetryAfterRefreshError extends Error {}
class AuthError extends Error {}

class Repository {
    constructor(options) {
        this.options = options;
        this.initialized = false;
    }

    initialize() {
        const options = this.options
        this.initialized = true;
        assert (options.store, "[repository] No store given, token authentication will not work.");
        this.store = options.store;
        this.router = options.router || this.store.$router;
        assert (this.router, "[repository] No router given, token authentication will not work.");

        this.store.registerModule('auth', {
            namespaced: true,
            state: initialState,
            mutations: mutations,
            actions: actions,
            getters: getters
        });

        const option_services = options.services || {};

        this.services = {...services, ...option_services};
        this.loginEndpoint = options.loginEndpoint || ["auth", "login"];
        this.logoutEndpoint = options.logoutEndpoint || ["auth", "logout"];
        this.refreshEndpoint = options.refreshEndpoint || ["auth", "refresh"];
        this.accountEndpoint = options.accountEndpoint || ["users", "me"];
        let baseUrl = options.baseUrl || "http://localhost";
        this.axios = axios.create({
            baseURL: baseUrl,
            timeout: 5000,
        });
    }

    async refreshToken() {
        /*
        ** Get a fresh JWT token using the refresh token
        */
        this.store.dispatch('auth/setIsRefreshing', true);
        const refresh_token = this.store.state.auth.refreshToken;
        if (refresh_token === null) {
            return Promise.reject("no refresh token")
        }
        const config = {
            headers: {
                Authorization: 'Bearer ' + this.store.state.auth.refresh_token
            },
            dontTryToRefresh: true
        };
        const response = await this.makeApiCall({
            service: this.refreshEndpoint[0],
            endpoint: this.refreshEndpoint[1],
            method: 'post',
            customOptions: config
        }).catch(error => {
            this.store.dispatch('auth/setIsRefreshing', false);
            throw error
        });
        const { access_token, expires } = response.data;
        this.setJWTToken(access_token, expires);
        this.store.dispatch('auth/setIsRefreshing', false);
        return Promise.resolve()
    }

    async setJWTToken(token, expires) {
        await this.store.dispatch('auth/setJWTToken', {token: token, expires: expires});
        // set timeout for jwt refresh
        const timeout = Math.round(expires*1000 - Date.now());
        setTimeout(async () => {

            await this.refreshToken().catch(error => {
                console.error(error)
                if (this.router.currentRoute.name !== "login") {
                    this.router.push({ name: "login" })
                }
                throw error
            });
        }, timeout)
    }

    async loginWith(username, password) {
        /*
        ** Make a basic auth request to the login endpoint
        ** Receive JWT and get user object
        */
        const config = {
            auth: {
                username: username,
                password: password
            }
        };
        const response = await this.makeApiCall({
            service: this.loginEndpoint[0],
            endpoint: this.loginEndpoint[1],
            method: 'post',
            customOptions: config
        }).catch(error => { throw error });
        const { access_token, expires, refresh_token } = response.data;
        this.setJWTToken(access_token, expires);
        await this.store.dispatch('auth/setRefreshToken', refresh_token);

        const user = await this.getUser()
            .catch(error => { return Promise.reject('Failed to fetch user: ' + error)});
        this.store.dispatch('auth/setUser', user);
        return response
    }

    async logout() {
        /*
        ** Send logout to auth API and get rid of all client data
         */
        await this.makeApiCall({
            service: this.logoutEndpoint[0],
            endpoint: this.logoutEndpoint[1],
            method: 'post'
        }).catch(error => { throw error });
        this.store.dispatch('auth/reset');
    }

    async getUser() {
        const config = {
            url: this.getUrl(this.accountEndpoint[0], this.accountEndpoint[1], {}),
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + this.store.state.auth.jwt_token.token
            }
        }
        const response = await this._makeAxiosCall(config);
        return response.data
    }

    getUrl(service, endpoint, params) {
        let url = this.services[service].url + this.services[service].endpoints[endpoint].url;
        const pattern = /:\w+/g;
        const keys = url.match(pattern);
        if (keys !== null && keys.length > 0) {
            for (let k in keys) {
                const key = keys[k].slice(1);
                if (params[key] === undefined || params[key] === null) {
                    throw new Error('Missing URL parameter ' + key);
                }
                url = url.replace(':' + key, params[key]);
            }

        }
        return url;
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async preCallChecks(service, endpoint, config) {
        if (this.services[service].endpoints[endpoint].protection === "jwt") {
            if (!this.store.getters['auth/loggedIn'])  {
                if (!this.store.state.auth.is_refreshing) {
                    // User not logged in and not refreshing
                    throw new AuthError('Cannot access protected resource, user not logged in.')
                }
                // waiting for refresh to eventually finish
                let counter = 0;
                while (counter < 3 && this.store.state.auth.is_refreshing) {
                    await this.sleep(500);
                    counter++;
                }
                if (!this.store.getters['auth/loggedIn']) {
                    // still not logged in, giving up
                    throw new AuthError('Cannot access protected resource, cannot refresh the token.')
                }
            }
            // is jwt still valid?
            if (Math.round(this.store.state.auth.jwt_token.expires*1000 - Date.now()) < 1000) {
                await this.refreshToken()
            }
            config['headers'] = {
                Authorization: 'Bearer ' + this.store.state.auth.jwt_token.token
            }
        }
        return config
    }

    makeApiCall(config) {
        const service = config.service || null;
        const endpoint = config.endpoint || null;
        const method = config.method || 'get';
        const data = config.data || {};
        const customOptions = config.customOptions || {};
        const retry = config.retry || false;
        const params = config.params || {};

        return this.preCallChecks(service, endpoint,{
            url: this.getUrl(service, endpoint, params),
            method: method,
            data: data
        })
            .then(config => {
                return new Promise(((resolve, reject) => {
                    this._makeAxiosCall({ ...config, ...customOptions }, retry)
                        .then(response => { resolve(response) })
                        .catch(error => {
                            if (error instanceof RetryAfterRefreshError) {
                                // retry after token refresh
                                return this.makeApiCall({
                                    service: service,
                                    endpoint: endpoint,
                                    params: params,
                                    method: method,
                                    data: data,
                                    customOptions: customOptions
                                })
                            } else {
                                reject(error)
                            }
                        })
                }))
            })
            .catch(error => {
                if (this.router.currentRoute.name !== "login" && error instanceof AuthError) {
                    this.router.push({name: "login"})
                }
                return Promise.reject(error)
            })
    }

    async _makeAxiosCall(config, retry) {
        retry = retry || false;
        const dontRefresh = config.dontTryToRefresh || false;
        return await this.axios(config)
            .catch(async (error) => {
                const { response } = error;
                if (response && response.status === 401 && this.store.state.auth.refresh_token !== null && !retry && !dontRefresh) {
                    // 401 error indicates that our JWT token is expired
                    // get rid of the JWT and set state to isRefreshing

                    // console.log("401 error intercepted...trying to get a fresh token");

                    this.store.dispatch('auth/resetJWTToken');
                    let refresh_error = null;
                    await this.refreshToken().catch(error => { refresh_error = error });
                    if (refresh_error === null) {
                        // retry calling myself
                        throw new RetryAfterRefreshError()
                    } else {
                        this.store.dispatch('auth/resetRefreshToken');
                        throw new AuthError()
                    }
                }
                throw error
            });
    }
}


// eslint-disable-next-line no-unused-vars
export default {
    install (Vue, args = {}) {
        Vue.prototype.$repository = new Repository(args);
        Vue.mixin({
            created() {
                if (!Vue.prototype.$repository.initialized) {
                    Vue.prototype.$repository.initialize();
                }
            }
        })

    }
}
