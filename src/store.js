export const initialState = {
    user: null,
    jwt_token: {
        token: null,
        expires: null
    },
    refresh_token: null,
    is_refreshing: false,
};

export const mutations = {
    reset (state) {
        state.user = null;
        state.jwt_token.token = null;
        state.jwt_token.expires = null;
        state.refresh_token = null;
        state.is_refreshing = false;
    },
    setJWTToken (state, { token, expires }) {
        state.jwt_token.token = token;
        state.jwt_token.expires = expires
    },
    resetJWTToken (state) {
        state.jwt_token.token = null;
        state.jwt_token.expires = null
    },
    setRefreshToken (state, refresh_token) {
        state.refresh_token = refresh_token
    },
    resetRefreshToken (state) {
        state.refresh_token = null;
    },
    setIsRefreshing (state, value) {
        state.is_refreshing = value
    },
    setUser (state, user) {
        state.user = user
    },
};

export const actions = {
    reset ({ commit }) {
        commit('reset')
    },
    setJWTToken ({ commit }, data) {
        commit('setJWTToken', data)
    },
    resetJWTToken ({ commit }) {
        commit('resetJWTToken')
    },
    setRefreshToken ({ commit }, data) {
        commit('setRefreshToken', data)
    },
    resetRefreshToken ({ commit }) {
        commit('resetRefreshToken')
    },
    setIsRefreshing ({ commit }, value) {
        commit('setIsRefreshing', value)
    },
    setUser ({ commit }, data) {
        commit('setUser', data)
    },
    resetUser ({ commit }) {
        commit('setUser', null)
    }

};

export const getters = {
    getUser (state) {
        return state.user
    },
    loggedIn (state) {
        return (state.user !== null)
    },
};
